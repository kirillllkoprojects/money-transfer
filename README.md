# Money transfer API #

### Explicit requirements: ###
1. You can use Java or Kotlin.
2. Keep it simple and to the point (e.g. no need to implement any authentication).
3. Assume the API is invoked by multiple systems and services on behalf of end users.
4. You can use frameworks/libraries if you like (except Spring), but don't forget about
requirement #2 and keep it simple and avoid heavy frameworks.
5. The datastore should run in-memory for the sake of this test.
6. The final result should be executable as a standalone program (should not require a
pre-installed container/server).
7. Demonstrate with tests that the API works as expected.

### Implicit requirements: ###
1. The code produced by you is expected to be of high quality.
2. There are no detailed requirements, use common sense.

### Technical stack ###

1. [Micronaut](https://micronaut.io/)
2. [Jooq](https://www.jooq.org/)
3. [H2](https://www.h2database.com/html/main.html)
3. [Flyway](https://flywaydb.org/)
4. [Swagger](https://swagger.io/)

### How to run ###

You can build application by ```./gradlew clean build```.

You can run application by ```./gradlew clean run```. 
 
Once you will run it, swagger documentation should be on ```http://localhost:8080/swagger-ui/#/```
