package com.revolut.money.transfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.money.transfer.dao.AccountDao;
import com.revolut.money.transfer.dto.Account;
import com.revolut.money.transfer.dto.CreateAccountRequest;
import com.revolut.money.transfer.dto.ErrorType;
import com.revolut.money.transfer.dto.OperationResult;
import com.revolut.money.transfer.dto.TransferRequest;
import com.revolut.money.transfer.service.TransferService;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest(application = Application.class)
public class MoneyTransferTest {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Inject
    AccountDao accountDao;

    @Inject
    TransferService transferService;

    @Inject
    @Client("/")
    RxHttpClient client;

    @BeforeEach
    void start() {
        accountDao.removeAllAccounts();
    }

    @Test
    public void should_create_account() {
        OperationResult result = client.toBlocking().retrieve(HttpRequest.POST("/account",
                CreateAccountRequest.builder().withId(1L).withAmount(BigDecimal.ONE).build()), OperationResult.class);
        assertEquals(result.getIsSuccess(), true);

        OperationResult getResult = client.toBlocking().retrieve(HttpRequest.GET("/account/1"), OperationResult.class);
        Account account = MAPPER.convertValue(getResult.getBody().get(), Account.class);
        assertEquals(result.getIsSuccess(), true);
        assertEquals(account.getId(), 1L);
        assertEquals(account.getAmount(), BigDecimal.ONE);
    }

    @Test
    public void should_get_account_by_id() {
        accountDao.create(1L, BigDecimal.valueOf(1000L));
        OperationResult<Account> retrieve = client.toBlocking().retrieve(HttpRequest.GET("/account/1"), OperationResult.class);
        Account account = MAPPER.convertValue(retrieve.getBody().get(), Account.class);
        assertEquals(account.getId(), 1L);
        assertEquals(account.getAmount(), BigDecimal.valueOf(1000L));
    }

    @Test
    public void should_successfully_transfer_money_between_accounts_in_concurrent_way() {

        accountDao.create(1L, BigDecimal.valueOf(1000L));
        accountDao.create(2L, BigDecimal.valueOf(1000L));
        accountDao.create(3L, BigDecimal.valueOf(1000L));


        CompletableFuture<OperationResult> result = CompletableFuture.supplyAsync(() ->
                transferService.transferMoney(TransferRequest.builder()
                        .withAccountSender(1L)
                        .withAccountReceiver(3L)
                        .withAmount(BigDecimal.TEN).build())
        );
        CompletableFuture<OperationResult> result2 = CompletableFuture.supplyAsync(() ->
                transferService.transferMoney(TransferRequest.builder()
                        .withAccountSender(1L)
                        .withAccountReceiver(3L)
                        .withAmount(BigDecimal.TEN).build())
        );
        CompletableFuture<OperationResult> result3 = CompletableFuture.supplyAsync(() ->
                transferService.transferMoney(TransferRequest.builder()
                        .withAccountSender(1L)
                        .withAccountReceiver(3L)
                        .withAmount(BigDecimal.TEN).build())
        );
        //at this moment if it goes straightforward we get 970 on 1st account and 1030 on 3rd account

        CompletableFuture<OperationResult> result4 = CompletableFuture.supplyAsync(() ->
                transferService.transferMoney(TransferRequest.builder()
                        .withAccountSender(3L)
                        .withAccountReceiver(2L)
                        .withAmount(BigDecimal.ONE).build())
        );
        //970 on 1st, 1001 on 2nd and 1029 on 3rd

        CompletableFuture<OperationResult> result5 = CompletableFuture.supplyAsync(() ->
                transferService.transferMoney(TransferRequest.builder()
                        .withAccountSender(2L)
                        .withAccountReceiver(1L)
                        .withAmount(BigDecimal.valueOf(999)).build())
        );
        //1971 on 1st, 2 on 2nd and 1029 on 3rd

        CompletableFuture<OperationResult> result6 = CompletableFuture.supplyAsync(() ->
                transferService.transferMoney(TransferRequest.builder()
                        .withAccountSender(2L)
                        .withAccountReceiver(1L)
                        .withAmount(BigDecimal.valueOf(2)).build())
        );
        // the result of concurrent operations should be 1971 on 1st, 0 on 2nd and 1029 on 3rd

        CompletableFuture.allOf(result, result2, result3, result4, result5, result6).join();

        Account account1 = accountDao.fetchById(1L).orElse(null);
        Account account2 = accountDao.fetchById(2L).orElse(null);
        Account account3 = accountDao.fetchById(3L).orElse(null);

        assertEquals(account1.getAmount(), BigDecimal.valueOf(1971L));
        assertEquals(account2.getAmount(), BigDecimal.valueOf(0L));
        assertEquals(account3.getAmount(), BigDecimal.valueOf(1029L));
    }

    @Test
    public void should_throw_error_if_not_enough_funds() {
        accountDao.create(1L, BigDecimal.valueOf(1L));
        accountDao.create(2L, BigDecimal.valueOf(1000L));
        OperationResult result = client.toBlocking().retrieve(HttpRequest.POST("/account/transfer",
                TransferRequest.builder().withAccountSender(1L).withAccountReceiver(2L).withAmount(BigDecimal.TEN).build()), OperationResult.class);
        assertEquals(result.getIsSuccess(), false);
        assertEquals(result.getError().get(), ErrorType.NOT_ENOUGH_FUNDS);
    }

    @Test
    public void should_throw_error_if_trying_to_transfer_on_same_account() {
        accountDao.create(1L, BigDecimal.valueOf(1L));
        OperationResult result = client.toBlocking().retrieve(HttpRequest.POST("/account/transfer",
                TransferRequest.builder().withAccountSender(1L).withAccountReceiver(1L).withAmount(BigDecimal.TEN).build()), OperationResult.class);
        assertEquals(result.getIsSuccess(), false);
        assertEquals(result.getError().get(), ErrorType.SAME_ACCOUNT_FAIL);
    }

    @Test
    public void should_throw_error_if_trying_to_get_not_created_account() {
        OperationResult result = client.toBlocking().retrieve(HttpRequest.GET("/account/1"), OperationResult.class);
        assertEquals(result.getIsSuccess(), false);
        assertEquals(result.getError().get(), ErrorType.ILLEGAL_ID);
    }
}
