package com.revolut.money.transfer.dao;

import com.revolut.money.transfer.dto.Account;
import com.revolut.money.transfer.tables.records.AccountsRecord;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.RecordMapper;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import static com.revolut.money.transfer.Tables.ACCOUNTS;

/**
 * Dao for work with accounts test
 */
@Singleton
public class AccountDao {
    private static final Logger log = LoggerFactory.getLogger(AccountDao.class);

    private final DSLContext dslContext;
    private final Configuration configuration;

    private static final RecordMapper<AccountsRecord, Account> recordMapper = record -> Account.builder()
            .withId(record.get(ACCOUNTS.ID))
            .withAmount(record.get(ACCOUNTS.AMOUNT))
            .build();

    public AccountDao(DSLContext dslContext, Configuration configuration) {
        this.dslContext = dslContext;
        this.configuration = configuration;
    }

    /**
     * Get account by id
     *
     * @param accountId - id of the account
     * @return - account with amount or exception if account does not exist
     */
    public Optional<Account> fetchById(@Nonnull Long accountId) {
        Objects.requireNonNull(accountId, "accountId");
        return dslContext.selectFrom(ACCOUNTS)
                .where(ACCOUNTS.ID.eq(accountId)).fetchOptional(recordMapper);
    }

    /**
     * Create account
     *
     * @param accountId - possible id of the account
     * @param amount    - amount of the account
     */
    public void create(@Nullable Long accountId, @Nonnull BigDecimal amount) {
        Objects.requireNonNull(amount, "amount");
        DSL.using(configuration).transaction(conf -> {
            DSL.using(conf).insertInto(ACCOUNTS)
                    .columns(ACCOUNTS.ID, ACCOUNTS.AMOUNT)
                    .values(accountId, amount)
                    //some problems with onConflict() on H2 db so did't make upsert.
                    // would like to do upsert method with PUT http, but H2 didn't think so...
                    .execute();
        });
    }

    /**
     * Transactional update of two accounts in process of transferring money
     *
     * @param senderAccount   - account which transfers money
     * @param receiverAccount - account which receives money
     */
    public void updateAccountsOnTransfer(@Nonnull Account senderAccount, @Nonnull Account receiverAccount) {
        Objects.requireNonNull(senderAccount, "senderAccount");
        Objects.requireNonNull(senderAccount, "receiverAccount");
        DSL.using(configuration).transaction(conf -> {
            DSL.using(conf).update(ACCOUNTS)
                    .set(ACCOUNTS.AMOUNT, senderAccount.getAmount())
                    .where(ACCOUNTS.ID.eq(senderAccount.getId()))
                    .execute();
            DSL.using(conf).update(ACCOUNTS)
                    .set(ACCOUNTS.AMOUNT, receiverAccount.getAmount())
                    .where(ACCOUNTS.ID.eq(receiverAccount.getId()))
                    .execute();
        });
        log.info("transfer done");
    }

    /**
     * Remove all accounts
     */
    public void removeAllAccounts() {
        DSL.using(configuration).transaction(conf -> {
            DSL.using(conf).truncate(ACCOUNTS).execute();
        });
    }
}
