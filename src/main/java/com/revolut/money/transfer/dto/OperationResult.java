package com.revolut.money.transfer.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

@Schema(name = "OperationResult", description = "Result of the operation")
public class OperationResult<T> {

    @Schema(description = "Flag of the successful operation", example = "true", required = true)
    private final Boolean isSuccess;

    @Schema(description = "Possible error message", example = "illegalId")
    private final ErrorType error;

    @Schema(description = "Possible response body")
    private final T body;

    @JsonCreator
    private OperationResult(@Nonnull @JsonProperty("isSuccess") Boolean isSuccess,
                            @Nullable @JsonProperty("error") ErrorType error,
                            @Nullable @JsonProperty("body") T body) {
        this.isSuccess = requireNonNull(isSuccess);
        this.error = error;
        this.body = body;
    }

    @Nonnull
    public Optional<ErrorType> getError() {
        return Optional.ofNullable(error);
    }

    @Nonnull
    public Optional<T> getBody() {
        return Optional.ofNullable(body);
    }

    @Nonnull
    public Boolean getIsSuccess() {
        return isSuccess;
    }

    @Override
    public String toString() {
        return "TransferRequest{" +
                "isSuccess=" + isSuccess +
                ", error=" + error +
                ", body=" + body +
                '}';
    }

    @Nonnull
    public static <T> Builder<T> builder() {
        return new Builder<>();
    }


    public static final class Builder<T> {
        private Boolean isSuccess;
        private ErrorType errorMessage;
        private T body;

        private Builder() {
        }

        public Builder<T> withIsSuccess(Boolean isSuccess) {
            this.isSuccess = isSuccess;
            return this;
        }

        public Builder<T> withErrorMessage(ErrorType errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public Builder<T> withBody(T body) {
            this.body = body;
            return this;
        }

        @Nonnull
        public OperationResult<T> build() {
            return new OperationResult<T>(isSuccess, errorMessage, body);
        }
    }
}
