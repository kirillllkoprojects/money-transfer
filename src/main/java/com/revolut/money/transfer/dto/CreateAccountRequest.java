package com.revolut.money.transfer.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.Optional;

@Schema(name = "CreateAccountRequest", description = "Request for create an account")
@Introspected
public class CreateAccountRequest {

    @Schema(description = "Id of account", example = "1")
    @Nullable
    private final @Min(1) Long id;

    @Schema(description = "Amount of future account", example = "1000")
    @Nullable
    private final BigDecimal amount;

    @JsonCreator
    public CreateAccountRequest(@Nullable @JsonProperty("id") Long id,
                                @Nullable @JsonProperty("amount") BigDecimal amount) {
        this.amount = amount;
        this.id = id;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("amount")
    public Optional<BigDecimal> getAmount() {
        return Optional.ofNullable(amount);
    }

    @Nonnull
    @JsonProperty("id")
    public Optional<Long> getId() {
        return Optional.ofNullable(id);
    }

    @Override
    public String toString() {
        return "UpsertAccountRequest{" +
                "amount=" + amount +
                ", id=" + id +
                '}';
    }

    public static final class Builder {
        private BigDecimal amount;
        private Long id;

        private Builder() {
        }

        public Builder withAmount(@Nullable BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Builder withId(@Nullable Long id) {
            this.id = id;
            return this;
        }

        @Nonnull
        public CreateAccountRequest build() {
            return new CreateAccountRequest(id, amount);
        }
    }
}
