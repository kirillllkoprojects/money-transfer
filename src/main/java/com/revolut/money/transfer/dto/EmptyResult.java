package com.revolut.money.transfer.dto;

/**
 * Empty result, when body is null
 */
public final class EmptyResult {

    private static final EmptyResult INSTANCE = new EmptyResult();

    private EmptyResult() {
    }

    public static EmptyResult getInstance() {
        return INSTANCE;
    }
}