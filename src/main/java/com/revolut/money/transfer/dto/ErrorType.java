package com.revolut.money.transfer.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "ErrorType", description = "Errors in operations")
public enum ErrorType {

    ILLEGAL_ID("illegalId"),

    SAME_ACCOUNT_FAIL("sameAccount"),

    NOT_ENOUGH_FUNDS("notEnoughFunds"),

    TECHNICAL_ERROR("technicalError");

    private String type;

    ErrorType(String type) {
        this.type = type;
    }

    @JsonValue
    public String getType() {
        return type;
    }
}
