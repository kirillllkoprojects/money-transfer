package com.revolut.money.transfer.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.annotation.Nonnull;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Schema(name = "TransferRequest", description = "Transfer request")
@Introspected
public class TransferRequest {

    @Schema(description = "Account id", example = "1", required = true)
    private final @NotNull @Min(1) Long accountSender;

    @Schema(description = "Account amount", example = "2", required = true)
    private final @NotNull @Min(1) Long accountReceiver;

    @Schema(description = "Account amount", example = "1000", required = true)
    private final @NotNull BigDecimal amount;

    @JsonCreator
    public TransferRequest(
            @Nonnull @JsonProperty("accountSender") Long accountSender,
            @Nonnull @JsonProperty("accountReceiver") Long accountReceiver,
            @Nonnull @JsonProperty("amount") BigDecimal amount
    ) {
        this.accountSender = accountSender;
        this.accountReceiver = accountReceiver;
        this.amount = amount;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("accountSender")
    public Long getAccountSender() {
        return accountSender;
    }

    @Nonnull
    @JsonProperty("accountReceiver")
    public Long getAccountReceiver() {
        return accountReceiver;
    }

    @Nonnull
    @JsonProperty("amount")
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "TransferRequest{" +
                "accountSender=" + accountSender +
                ", accountReceiver=" + accountReceiver +
                ", amount=" + amount +
                '}';
    }

    public static final class Builder {
        private Long accountSender;
        private Long accountReceiver;
        private BigDecimal amount;

        private Builder() {
        }

        public Builder withAccountSender(@Nonnull Long accountSender) {
            this.accountSender = accountSender;
            return this;
        }

        public Builder withAccountReceiver(@Nonnull Long accountReceiver) {
            this.accountReceiver = accountReceiver;
            return this;
        }

        public Builder withAmount(@Nonnull BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        @Nonnull
        public TransferRequest build() {
            return new TransferRequest(accountSender, accountReceiver, amount);
        }
    }
}
