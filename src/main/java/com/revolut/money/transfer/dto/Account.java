package com.revolut.money.transfer.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

import static java.util.Objects.requireNonNull;

@Schema(name = "Account", description = "Account with money")
public class Account {

    @Schema(description = "Account id", example = "1", required = true)
    @Nonnull
    private final Long id;

    @Schema(description = "Account amount", example = "1000", required = true)
    @Nonnull
    private final BigDecimal amount;

    @JsonCreator
    private Account(
            @Nonnull @JsonProperty("id") Long id,
            @Nonnull @JsonProperty("amount") BigDecimal amount
    ) {
        this.id = requireNonNull(id, "id");
        this.amount = requireNonNull(amount, "amount");
    }

    /**
     * Создает новый объект билдера для {@link Account}
     */
    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @Nonnull
    @JsonProperty("amount")
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", amount=" + amount +
                '}';
    }

    /**
     * Билдер для {@link Account}
     */
    public static final class Builder {
        private Long id;
        private BigDecimal amount;

        private Builder() {
        }

        public Builder withId(@Nonnull Long id) {
            requireNonNull(id, "id");
            this.id = id;
            return this;
        }

        public Builder withAmount(@Nonnull BigDecimal amount) {
            requireNonNull(amount, "amount");
            this.amount = amount;
            return this;
        }

        /**
         * Собрать объект
         */
        @Nonnull
        public Account build() {
            return new Account(id, amount);
        }
    }
}
