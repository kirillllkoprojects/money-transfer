package com.revolut.money.transfer;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(
                title = "Money transfer API",
                version = "1.0",
                description = "Revolut test API for transferring money between accounts",
                contact = @Contact(url = "https://bitbucket.org/kirillllkoprojects/",
                        name = "Kirill Zubkov", email = "zubkov93@mai.ru")
        )
)
public class Application {
    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}