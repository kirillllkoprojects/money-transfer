package com.revolut.money.transfer.controller;

import com.revolut.money.transfer.dto.Account;
import com.revolut.money.transfer.dto.CreateAccountRequest;
import com.revolut.money.transfer.dto.EmptyResult;
import com.revolut.money.transfer.dto.OperationResult;
import com.revolut.money.transfer.dto.TransferRequest;
import com.revolut.money.transfer.service.AccountService;
import com.revolut.money.transfer.service.TransferService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.validation.Validated;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Controller("/account")
@Validated
@Tag(name = "Account controller")
public class AccountController {

    private final TransferService transferService;
    private final AccountService accountService;

    public AccountController(TransferService transferService, AccountService accountService) {
        this.transferService = transferService;
        this.accountService = accountService;
    }

    @Get("/{id}")
    @Operation(summary = "Gets account by id")
    public HttpResponse<OperationResult<Account>> getById(@PathVariable @NotNull @Min(1) Long id) {
        return HttpResponse.ok(accountService.getById(id));
    }

    @Post
    @Operation(summary = "Creates account")
    public HttpResponse<OperationResult<EmptyResult>> createAccount(@Valid @Body CreateAccountRequest createAccountRequest) {
        return HttpResponse.ok(accountService.createAccount(createAccountRequest));
    }

    @Post("/transfer")
    @Operation(summary = "Transfers money from one account to another")
    public HttpResponse<OperationResult<EmptyResult>> transfer(@Valid @Body TransferRequest transferRequest) {
        return HttpResponse.ok(transferService.transferMoney(transferRequest));
    }
}