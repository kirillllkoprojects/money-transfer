package com.revolut.money.transfer.controller;

import com.revolut.money.transfer.dto.ErrorType;
import com.revolut.money.transfer.dto.OperationResult;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

import javax.inject.Singleton;

/**
 * Exception handler for mapping exception to common response
 */
@Produces
@Singleton
@Requires(classes = {RuntimeException.class, ExceptionHandler.class})
public class GlobalExceptionHandler implements ExceptionHandler<RuntimeException, HttpResponse<OperationResult<String>>> {

    @Override
    public HttpResponse<OperationResult<String>> handle(HttpRequest request, RuntimeException exception) {
        return HttpResponse.serverError(OperationResult.<String>builder()
                .withIsSuccess(false)
                .withErrorMessage(ErrorType.TECHNICAL_ERROR)
                .withBody(exception.getMessage()).build());
    }

}
