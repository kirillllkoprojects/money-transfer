package com.revolut.money.transfer.service;

import com.revolut.money.transfer.dao.AccountDao;
import com.revolut.money.transfer.dto.Account;
import com.revolut.money.transfer.dto.EmptyResult;
import com.revolut.money.transfer.dto.ErrorType;
import com.revolut.money.transfer.dto.OperationResult;
import com.revolut.money.transfer.dto.TransferRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Service for transferring amount of money between accounts
 */
@Singleton
public class TransferService {

    private static final Logger log = LoggerFactory.getLogger(TransferService.class);

    private final Map<Long, Lock> lockMap = new ConcurrentHashMap<>();
    private final AccountDao accountDao;

    public TransferService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    /**
     * Transfer money from one account to another
     *
     * @param transferRequest - request with ids of accounts and amount to transfer
     * @return - result of transfer operation
     */
    public OperationResult<EmptyResult> transferMoney(TransferRequest transferRequest) {
        Long accountIdFrom = transferRequest.getAccountSender();
        Long accountIdTo = transferRequest.getAccountReceiver();
        if (accountIdFrom.equals(accountIdTo)) {
            log.error("transfer failed, could not transfer on same account");
            return buildFailedOperationResult(ErrorType.SAME_ACCOUNT_FAIL);
        }

        long min = Math.min(accountIdFrom, accountIdTo);
        long max = Math.max(accountIdFrom, accountIdTo);

        try {
            lockMap.computeIfAbsent(min, account -> new ReentrantLock()).lock();
            lockMap.computeIfAbsent(max, account -> new ReentrantLock()).lock();

            Account accountFrom = accountDao.fetchById(accountIdFrom)
                    .orElseThrow(() -> new IllegalArgumentException("illegal id"));
            Account accountTo = accountDao.fetchById(accountIdTo)
                    .orElseThrow(() -> new IllegalArgumentException("illegal id"));

            if (accountFrom.getAmount().compareTo(transferRequest.getAmount()) < 0) {
                log.error("transfer failed, not enough funds for transfer");
                return buildFailedOperationResult(ErrorType.NOT_ENOUGH_FUNDS);
            }
            Account updatedAccountFrom = Account.builder()
                    .withId(accountIdFrom)
                    .withAmount(accountFrom.getAmount().subtract(transferRequest.getAmount()))
                    .build();
            Account updatedAccountTo = Account.builder()
                    .withId(accountIdTo)
                    .withAmount(accountTo.getAmount().add(transferRequest.getAmount()))
                    .build();
            accountDao.updateAccountsOnTransfer(updatedAccountFrom, updatedAccountTo);

            log.info("transfer successfully confirmed");
            return OperationResult.<EmptyResult>builder()
                    .withIsSuccess(true).build();
        } catch (Exception e) {
            log.error("technical error, error=", e);
            return buildFailedOperationResult(ErrorType.TECHNICAL_ERROR);
        } finally {
            lockMap.get(max).unlock();
            lockMap.get(min).unlock();
        }
    }

    private static OperationResult<EmptyResult> buildFailedOperationResult(ErrorType errorType) {
        return OperationResult.<EmptyResult>builder()
                .withIsSuccess(false)
                .withErrorMessage(errorType).build();
    }
}
