package com.revolut.money.transfer.service;

import com.revolut.money.transfer.dao.AccountDao;
import com.revolut.money.transfer.dto.Account;
import com.revolut.money.transfer.dto.CreateAccountRequest;
import com.revolut.money.transfer.dto.EmptyResult;
import com.revolut.money.transfer.dto.ErrorType;
import com.revolut.money.transfer.dto.OperationResult;

import javax.inject.Singleton;
import java.math.BigDecimal;

/**
 * Service for creating and reading accounts
 */
@Singleton
public class AccountService {

    private final AccountDao accountDao;

    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    /**
     * Create account
     *
     * @param createAccountRequest - request for create
     * @return - result of the operation
     */
    public OperationResult<EmptyResult> createAccount(CreateAccountRequest createAccountRequest) {
        accountDao.create(
                createAccountRequest.getId().orElse(null),
                createAccountRequest.getAmount().orElse(BigDecimal.ZERO));
        return OperationResult.<EmptyResult>builder().withIsSuccess(true).build();
    }

    /**
     * Get account with amount by id
     *
     * @param id - id of the account
     * @return - account
     */
    public OperationResult<Account> getById(Long id) {
        return accountDao.fetchById(id)
                .map(actualAccount ->
                        OperationResult.<Account>builder()
                                .withIsSuccess(true)
                                .withBody(actualAccount).build())
                .orElseGet(() -> OperationResult.<Account>builder()
                        .withIsSuccess(false)
                        .withErrorMessage(ErrorType.ILLEGAL_ID).build());
    }
}
